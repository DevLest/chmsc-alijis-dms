<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

class Memo extends Model
{
    protected $table = 'memos';

    public static function postMemo(Request $request)
    {
        $title = $request->get('posttitle');
        $date = $request->get('postexp');
        $content = $request->get('postcontent');
        $url = $request->get('posturl');
        $blob = $request->get('blobimage');

        $memo = new Memo;

        $memo->title = $title;
        $memo->content = $content;
        $memo->imgpath = $url;
        $memo->blob = $blob;
        $memo->expiration = $date;
        
        // dd($memo);
        $memo->save();

        return $memo;
    }
}

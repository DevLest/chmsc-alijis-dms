<?php

namespace App\Http\Controllers;

use App\Memo;
use App\User;
use App\Receivers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;
use thiagoalessio\TesseractOCR\TesseractOCR;

class PostMemo extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return View::make('layouts.page_templates.memo.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $output = [];

        $output['message'] = (new TesseractOCR($request->fileUpload))->run();

        $file = $request->fileUpload;
        $filename = date("Y_m_d_-_h_i_sa-").$file->getClientOriginalName();
        $file->move(public_path("images"), $filename);

        $imageFileType = strtolower(pathinfo($file->getClientOriginalName(),PATHINFO_EXTENSION));
        $image_base64 = base64_encode(file_get_contents(public_path("images").'\\'.$filename) );
        $image = 'data:image/'.$imageFileType.';base64,'.$image_base64;

        $output['path'] = '/images/' . $filename;
        $output['blobimage'] = $image;

        return View::make('layouts.page_templates.memo.index', $output);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $id = explode('+', $id);
        $output = Memo::where('id', $id[1])->first();

        return View::make('layouts.page_templates.memo.view', $output);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function post(Request $request)
    {
        $memo = Memo::postMemo($request);

        foreach (User::all() as $user) 
        {
            Receivers::saveData($user->id, $memo->id);
        }

        return View::make('layouts.page_templates.memo.index');
    }

    public function getNotifCount(Request $request)
    {
        return response()->json(Receivers::where('userId', $request->id)->where('received', false)->count());
    }
}

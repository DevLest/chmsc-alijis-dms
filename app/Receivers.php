<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Receivers extends Model
{
    protected $table = "receivers";

    public static function saveData($user, $memo){
        $data = new Receivers();
        $data->userId = $user;
        $data->memoId = $memo;

        return $data->save();
    }
}

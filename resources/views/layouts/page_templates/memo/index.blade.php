@extends('layouts.app', ['activePage' => 'postMemo', 'titlePage' => 'Post Meno'])

@section('content')
	<div class="content">
		<div class="container-fluid">
			<form class="form" method="POST" action="{{ route('post.store') }}" enctype="multipart/form-data">
				@csrf
				<div class="row">
					<div class="col-5">
						<div class="bmd-form-group mt-3">
							<div class="input-group">
								<div class="input-group-prepend">
									<span class="input-group-text">
										<i class="material-icons">attach_file</i>
									</span>
								</div>
								<input type="file" name="fileUpload" id="fileUpload" class="form-control" placeholder="Upload File" value="" required>
							</div>
						</div>
					</div>
					<div class="col-2">
						<button type="submit" class="btn btn-danger">Scan Image</button>
					</div>
				</div>
			</form>
			<form class="form" method="POST" action="{{ route('post.post') }}" enctype="multipart/form-data">
				@csrf
				<br><br>
				<div class="align-items-center" style="width:50%;">
					<div class="bmd-form-group mt-3">
						<div class="input-group">
							<div class="input-group-prepend"><span class="input-group-text">Post Title</span></div>
							<input type="text" name="posttitle" id="posttitle" class="form-control" placeholder="Title">
						</div>
					</div>
					
					<div class="bmd-form-group mt-3">
						<div class="input-group">
							<div class="input-group-prepend"><span class="input-group-text">Expiration</span></div>
							<input type="date" name="postexp" id="postexp" class="form-control" placeholder="Title" >
						</div>
					</div>

					<div class="bmd-form-group mt-3">
						<div class="input-group">
							<div class="input-group-prepend"><span class="input-group-text">Content</span></div>
							<textarea rows="10" cols="100" class="form-control">
								{{ (isset($message) && $message != "") ? $message : ""  }}
							</textarea>
							<input type="text" name="postcontent" id="postcontent" hidden value="{{ (isset($message) && $message != "") ? $message : ""  }}">
						</div>
					</div>
					
					<div class="bmd-form-group mt-3">
						<br><br>
						<div class="input-group">
							<input type="text" name="posturl" id="posturl" value="{{ (isset($path) && $path != "") ? $path : ""  }}" hidden>
							<input type="text" name="blobimage" id="blobimage" value="{{ (isset($blobimage) && $blobimage != "") ? $blobimage : ""  }}" hidden>
							<img class="img-fluid" src="{{ (isset($path) && $path != "") ? $path : "" }}" alt="No Content">
						</div>
					</div>
					
					<br><br>
					<button type="submit" class="btn btn-danger" style="width: 100%">Post Memo</button>
				</div>
			</form>
		</div>
	</div>
@endsection
@extends('layouts.app', ['activePage' => 'postMemo', 'titlePage' => 'Post Meno'])

@section('content')
	<div class="content">
		<div class="container-fluid">
            <div class="align-items-center" style="width:50%;">
                <div class="bmd-form-group mt-3">
                    <div class="input-group">
                        <div class="input-group-prepend"><span class="input-group-text">Post Title</span></div>
                        <input type="text" name="posttitle" id="posttitle" class="form-control" placeholder="Title" value="{{$title}}">
                    </div>
                </div>
                <div class="bmd-form-group mt-3">
                    <div class="input-group">
                        <div class="input-group-prepend"><span class="input-group-text">Expiration Date</span></div>
                        <input type="text" name="posttitle" id="posttitle" class="form-control" placeholder="Title" value="{{$expiration}}">
                    </div>
                </div>

                <br><br>
                <div class="bmd-form-group mt-12">
                    <div class="input-group">
                        <div class="input-group-prepend"><span class="input-group-text">Content</span></div>
                        <textarea rows="15" cols="200" class="form-control">
                            {{ (isset($content) && $content != "") ? $content : ""  }}
                        </textarea>
                    </div>
                </div>
                
                <div class="bmd-form-group mt-3">
                    <br><br>
                    <div class="input-group">\
                        <img class="img-fluid" src="{{ (isset($blob) && $blob != "") ? $blob : "" }}" alt="No Content">
                    </div>
                </div>
                
                <br><br>
                <button type="submit" class="btn btn-danger" style="width: 100%">Receive Memo</button>
            </div>
		</div>
	</div>
@endsection
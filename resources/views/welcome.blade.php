@extends('layouts.app', ['class' => 'off-canvas-sidebar', 'activePage' => 'home', 'title' => __('CHMSC DMS')])

@section('content')
<div class="container" style="height: auto;">
  <div class="row justify-content-center">
      <div class="col-lg-7 col-md-8">
          <h2 class="text-white text-center"> Welcome to</h2>
          <br>
          <h1 class="text-white text-center">
            <b>CARLOS HILADO MEMORIAL STATE COLLEGE - ALIJIS</b>
            <br><br> Data Management System
          </h1>
      </div>
  </div>
</div>
@endsection
